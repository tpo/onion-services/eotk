# Backing up Onionspray

This is a quick backup guide about what to backup in your Onionspray
installation.

Onionspray is not difficult to backup, since it's just an Onion Services
rewriting proxy. You basically need to save .onion keys, HTTPS certificates and
configuration files.

## Backup

!!! tip "Combining manual with automated backups"

    When backing up Onionspray, you can combine:

    1. A manual backup whenever you add/update projects, sites, keys and
       certificates.
    2. A periodic and automatic backup.

!!! tip "Encrypted backups"

    We recomend that you do encrypted backups, and that you also backup the
    passphrases/keys to unlock this backup.

### The simpler backup approach

* **If unsure, backup the your whole Onionspray installation, in every server
  it's installed** .

### A more complete approach

If you want to really pick only what's really needed to rebuild you setup,
focus in backing up only the following:

* All your remote [softmap](balance/softmaps.md) workers/mirrors:
  * These will reside in the `mirrors/` folder after you use the `onionspray
    mirror` and `onionspray backup` commands.

* All your .onion addresses keys and configurations, consisting in:
  * The `projects/` folder (and especially each `ssl` folder from every project,
    like `projects/<project-name>/ssl`).
  * The `secrets` folder.
  * All `.conf` and `.tconf` files.

## Restore

!!! warning "Test your backups!"

    Please make sure to test your backups and have a restoration procedure
    ready and tested.

    The restoration procedure depends on how you do your backups and is
    currently beyond the scope of this guide, but we would be glad in receiving
    a merge request improving this documentation.
