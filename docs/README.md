# Onionspray

![Onionspray Logo](assets/logo.jpg "Onionspray")

## Introduction

[Onionspray][] is a tool to _setup [Onion Services][] for existing public
websites, working as a HTTPS rewriting proxy_.

Onionspray is a fork of Alec Muffett's [EOTK][], with many enhancements but
retaining compatibility, and relying on [C Tor][] until an alternative in
[Arti][] is available.

Onionspray works as a proxy between an Onion Service connection and the
website, bringing many benefits to operators:

* Onionspray is a self-contained software with everything needed to configure
  an onionsite.

* There's no need to adapt existing setups: Onionspray can be installed in a
  separate environment, and no changes are usually needed in the website.

The result is essentially a "Onion Services in the middle" proxy; you should
set them up only for your own sites, or for sites which do not require login
credentials of any kind.

[Onionspray]: https://gitlab.torproject.org/tpo/onion-services/onionspray/
[Onion Services]: https://community.torproject.org/onion-services/
[EOTK]: https://github.com/alecmuffett/eotk
[C Tor]: https://gitlab.torproject.org/tpo/core/tor
[Arti]: https://arti.torproject.org

## Use cases

Onionspray is intended to:

* _Setup [Onion Services][] for existing public websites_ that are already
  accessible through the internet, providing an additional protocol layer for
  accessing them.
* _Protect the user location information_ to be provided by default, by relying
  in the [Onion Services][] technology.
* _Protect the website against censorship_, by offering a way to access them
  that is censorship proof as long as both the website and users can access the
  Tor network.

## Non-use cases

Onionspray **is not intended to**:

* Setup a "pure" Onion Service website, i.e, it's not meant to setup a website
  that is only available through the Tor network.
* Protect the website identity or location, given that Onionspray is a rewriting
  proxy. Read [this note for details](security/anonymity.md).

## How it works

Onionspray works by setting up a HTTPS rewriting proxies between existing sites
an Tor users connecting through an Onion Service.

```mermaid
graph LR
  C[Client] -- .onion address via Tor Network --> O[Onionspray CDN] -- HTTPS through the Internet --> U[Upstream site]
```

The proxy is mainly intended to replace regular domain names with their .onion
counterparts, offering a seamless experience to users.

## Features

* The installation is non-intrusive and non-disruptive, as there's no
  need to changing existing website setups: the Onion Services infrastructure
  can be provisioned in separate systems, apart from where the site is located.

* It can run anywhere, in a laptop, a single board computer, in
  servers; as a standalone program, or as a container.

* Support for [load balancing setups](guides/balance/README.md), acting as
  an "Onion Service [CDN][]".

* No need for administrative privileges to run: Onionspray can be installed
  and run as a regular user.

* Can be installed in a variety of systems, from Debian and Ubuntu to FreeBSD
  and macOS.

* It's compatible with [EOTK][]: configuration, keys and certificates can be
  easily [migrated](migrating.md) from [EOTK][], and the `onionspray` command
  is compatible with the `eotk` script.

* Onionspray will be supported as long as [C Tor][] is supported.

[CDN]: https://en.wikipedia.org/wiki/Content_delivery_network
