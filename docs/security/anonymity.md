# Onionspray and website anonymity

**The presumed use-case of Onionspray is that you have an already-public
website you wish to give it a corresponding Onion address. This, by default
will already offers a protection the user location to be disclosed by
default.**

But what to say about the website location and identity?

A lot of people mistakenly believe that Tor Onion Networking is mainly about
anonymity -- which is incorrect, since it also includes:

* Extra privacy.
* Identity/surety of to whom you are connected.
* Freedom from oversight/network surveillance.
* Anti-blocking, and...
* Enhanced integrity/tamper proofing.

... none of which are the same as "anonymity", but all of which are valuable
qualities to add to communications.

Further: setting up an Onion address can provide less contention, more speed &
more bandwidth to people accessing your site than they would get by using Tor
"Exit Nodes".

If you set up Onionspray in its intended mode then your resulting site is almost
certainly not going to be anonymous; for one thing your brand name (etc) will
likely be plastered all over it.

**Users location will still get a protection by default** (as long as they
don't identify themselves by filling a form etc), but Onionspray is not
intended to offer the same protection the websites, as it assumes the website
is already public.

If you want to set up a server which includes anonymity **as well as**
all of the aforementioned qualities, you want want to be reading some
other references:

* [Building a Basic "Production" Onion Server on Ubuntu][].
* [What other attacks are there against Onion Services?][].

You may also be interested in setting up the [Vanguards Addon][].

[Building a Basic "Production" Onion Server on Ubuntu]: https://github.com/alecmuffett/the-onion-diaries/blob/master/basic-production-onion-server.md
[What other attacks are there against Onion Services?]: https://github.com/mikeperry-tor/vanguards/blob/master/README_SECURITY.md
[Vanguards Addon]: https://github.com/mikeperry-tor/vanguards/tree/master
