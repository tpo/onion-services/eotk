#!/bin/sh -x

# platform-independent lib.sh
cd `dirname $0` || exit 1
opt_dir=`pwd`
. ./lib.sh || exit 1

# installation dependencies
install_deps="
lsb-release
"

# platform dependencies
shared_deps="
openresty
tor
make
onionbalance
mkcert
ca-certificates
"

# installation requirements
sudo add-apt-repository universe
sudo apt update
sudo apt install -y $install_deps || exit 1

# install openresty
SetupOpenRestyAptRepository ubuntu || exit 1

# install tor
SetupTorAptRepository ubuntu || exit 1

# disable nginx
if which systemctl > /dev/null && systemctl list-unit-files nginx.service > /dev/null; then
  sudo systemctl disable nginx
  sudo systemctl stop nginx
fi

# install everything
sudo apt update
sudo apt install -y $shared_deps || exit 1

# done
exit 0
